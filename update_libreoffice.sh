#!/bin/bash

if [[ ! -d ./env ]]
then
	echo 'create venv first\n\
"python3 -m venv env  && . ./env/bin/activate && pip3 install -r requirements.txt"'
	exit 1
fi

OUT_DIR='/media/data/programs/linux/libreoffice'
. ./env/bin/activate
LIBREOFFICE_PATH="$(which soffice)"
if [[ ! "$LIBREOFFICE_PATH" ]] && [[ -f ./libreoffice_path ]]
then
	LIBREOFFICE_PATH="$(cat ./libreoffice_path)"
else
	echo "Enter path to soffice"
	read LIBREOFFICE_PATH
	echo $LIBREOFFICE_PATH > ./libreoffice_path
fi
export LIBREOFFICE_DOWNLOAD_DIR=${OUT_DIR} && export LIBREOFFICE_PATH=${LIBREOFFICE_PATH} && python3 update_libreoffice.py
deactivate
