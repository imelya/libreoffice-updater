# Script for updating libreoffice on linux (debain).

## Requirements

- [Python](https://www.python.org) >= 3.6
- [python3-venv](https://packages.debian.org/stable/python3-venv)

## Usage

- Clone repository

```bash
git clone https://bitbucket.org/imelya/libreoffice-updater.git && cd libreoffice-updater
```

- Activate Virtualenv

```bash
python3 -m venv env
. ./env/bin/activate
```

- Install requirements

```bash
pip install -r requirements.txt
```

- Deactivate Virtualenv

```bash
deactivate
```

- Change if necessary download dir at `update_libreoffice.sh`
- Launch

```bash
./update_libreoffice.sh
```
