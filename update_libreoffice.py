import os
import subprocess
import tarfile
import time
import requests
from bs4 import BeautifulSoup
from tqdm import tqdm

BASE_URL = 'https://download.documentfoundation.org/libreoffice/stable'
FILES = ('', 'langpack', )
OUT_DIR = os.environ['LIBREOFFICE_DOWNLOAD_DIR']
LIBREOFFICE_PATH = os.environ['LIBREOFFICE_PATH']
YES = ('y', 'Y', 'yes', 'Yes', 'YES')


def make_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def get_version():
    response = requests.get(BASE_URL)
    soup = BeautifulSoup(response.text, 'lxml')
    versions = []
    for row in soup.table.findAll('a', href=True):
        versions.append(row)
    version = versions[-1]['href'].replace('/', '')
    return version


def get_file(version, langpack):
    langpack = '' if not langpack else '_langpack_ru'
    url = f'{BASE_URL}/{version}/deb/x86_64/LibreOffice\
_{version}_Linux_x86-64_deb{langpack}.tar.gz'
    name = url.split('/')[-1]
    with requests.get(url, stream=True) as response:
        response.raise_for_status()
        with open(f'{OUT_DIR}/{name}', 'wb') as fd:
            for chunk in tqdm(response.iter_content(chunk_size=8192)):
                fd.write(chunk)
    print('='*10)
    print(name, 'saved')
    return name


def sleep():
    for i in range(1, 4):
        print(f'{i}..')
        time.sleep(1)


def confirm(new_version, current_version):
    if new_version.split('.')[:3] == current_version.split('.')[:3]:
        question = f'Latest version {new_version} installed.\
Install anyway? (y/n)\n'
    else:
        question = f'Current version is {current_version},\
new version is {new_version}. Install it? (y/n)\n'
    answer = input(question)
    out = True if answer in YES else False
    return out


def unpack_file(name):
    with tarfile.open(f'{OUT_DIR}/{name}') as tar:
        tar.extractall(OUT_DIR)
        folder = tar.getmembers()[0].name
    print('='*10)
    print(name, 'unpacked')
    return folder


def install(name):
    os.system(f'sudo dpkg -i {OUT_DIR}/{name.replace(".tar.gz", "")}/DEBS/*.deb')


def check_version():
    try:
        version = subprocess.check_output([LIBREOFFICE_PATH,
                                           '--version']).decode().split()[1]
    except FileNotFoundError:
        print('Wrong path to libreoffice.\nCheck "./libreoffice_path"')
        exit()
    return version


if __name__ == '__main__':
    try:
        new_version = get_version()
        current_version = check_version()
        if not confirm(new_version, current_version):
            exit()
        make_dir(OUT_DIR)
        for _file in FILES:
            name = get_file(new_version, _file)
            sleep()
            name = unpack_file(name)
            sleep()
            install(name)
    except KeyboardInterrupt:
        exit()
